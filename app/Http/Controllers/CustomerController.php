<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Customer;

class CustomerController extends Controller
{
     public function pdf()
     {
         $rows = Customer::all();

         $pdf = PDF::loadView('customers', compact('rows'));
         return $pdf->download('customers.pdf');
     }


    //  https://github.com/barryvdh/laravel-dompdf
    // https://getbootstrap.com/docs/4.4/getting-started/introduction/
    // https://github.com/fzaninotto/Faker#fakerprovideren_uscompany
}
