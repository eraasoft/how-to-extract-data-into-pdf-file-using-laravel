<?php

use Faker\Generator as Faker;

$factory->define(App\Customer::class, function (Faker $faker) {
    return [
        'name'=> $faker->name,
        'company_name'=> $faker->company,
        'email'=> $faker->email,
        'phone'=> $faker->phoneNumber,
        'code'=> $faker->randomDigit,
    ];
});
